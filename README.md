# About
Simple interactive online Python tutorial
for students at St. Augustine's College in Cape Coast
Site design proudly stolen from [Nishant Shukla's Haskell tutorial site]("http://shuklan.com/haskell/")


## URLS
* /announcements
* /lectures/01-12.html
* /discussions 


## Built with
* [Mojolicious]("http://mojolicio.us") - Evil Perl web framework
* [Twitter Bootstrap]("http://twitter.github.com/bootstrap") - UI toolkit that provides simple and flexible HTML,
CSS, and Javascript to implement popular user interface components and
interactions. Made at and used by Twitter
* [JQuery]("http://www.jquery.com") - The other libs depend on it
* [HumaneJS](wavded.github.com/humane-js) - A simple, modern browser notification system


## TODO
* make site dynamic
* add social sharing plugins
* build discussions page


## Contributing
1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Added an awesome new feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
